import pefile

file1 = open("text1", "w")
file1.write("What's Montague? It is nor hand, nor foot,\nNor arm, nor face, nor any other part \
\nBelonging to a man. O, be some other name.\nWhat's in a name? That which we call a rose \
\nBy any other name would smell as sweet;")
file1.close()
file1 = open("text1", "r")
print("Number of words name: " + str(file1.read().count("name")))

file1.close()

file1 = open("text1", "r")
file2 = open("text2", "w")
file2.write(file1.read().replace("name", "surname".title()))
file1.close()
file2.close()

inp = input("Enter the file you want to open (file1, file2, both): ")
while inp != "stop":
    if inp == "file1":
        file1 = open("text1", "r")
        print("Text from file 1:\n" + file1.read())
        file1.close()
        break
    elif inp == "file2":
        file2 = open("text2", "r")
        print("Text from file 2:\n" + file2.read())
        break
    elif inp == "both":
        file1 = open("text1", "r")
        file2 = open("text2", "r")
        print("Text from file 1:\n", "\n" + file1.read())
        print("\n")
        print("Text from file 2:\n", "\n" + file2.read())
        file1.close()
        file2.close()
        break
    else:
        inp = input("Try another (file1, file2, both): ")

    print("You have stopped the code.")
    break