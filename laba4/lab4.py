# import math


# def radium():
#    rad = input("Enter radius: ")
#    s = math.pi * pow(int(rad), 2)
#    print("Area of circle with radius: %s equals %s. (older format)" % (rad, s))
#    print("Area of circle with radius: {} equals {}. (newer format)".format(rad, s))
#    print("-----------------------------------------------------------------------")
#    number1 = 156
#    print(f"Число в восьмеричном представлении: {number1:#o}. \nЧисло в шестнадцатеричном представлении: {number1:#x}."
#          f"\nЧисло с плавающей точкой с экспонентой: {number1:#e}. \nЧисло в десятичном представлении {number1:}.")

# radium()
import re


def printer():
    file1 = open("file.txt", "r")
    file2 = open("file2.txt", "w")
    f_finder = re.findall(r"@\w+.\w+.\w+", file1.read()) #Заменил на регулярку.
    file2.write(str(f_finder))
    print(f_finder)
    print("Host`s name was replaced!")
    print("Enter the file you want to show (file1, file2, both): ")
    print("You can also use command stop to end the code!")
    req = input("Enter: ")
    req.lower()
    while req != "stop":
        if req == "file2":
            with open("file2.txt", "r"):
                print(file2.read())
                print("Success!")
                break
        elif req == "file1":
            with open("file.txt", "r"):
                print(file1.read())
                print("Success!")
                break
        elif req == "both":
            with open("file.txt", "r"):
                print("File 1: " + file1.read())
            with open("file2.txt", "r"):
                print("File 2: " + file2.read())
            print("\nSuccess!")
            break
        else:
            break


printer()