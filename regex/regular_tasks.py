import re


with open("classes_2_regexps_data_student_essays.txt", "r") as file:
    text = file.read()
mistakes = re.findall(r"(?i)\ba\b[\s\n]+[aeiiou]\w+", text)
#print(mistakes)

### result: ['A a', 'a e']

mistakes = re.findall(r"(?i)(can|could|shall|might|may|must|need|has to|have to)\s\bto\b", text)
#print(mistakes)

### result: ['can', 'could', 'could', 'could', 'can']

mistakes = re.findall(r"(?i)\d[\s\n]+\bam\b|\bpm\b", text)
#print(mistakes)


### result: []

new = re.sub(r"(?i)\ba\b(\s\b[aueio])", "an\\1", text)
print(new)

### result: ----

with open("classes_2_regexps_data_format-date.txt", "r") as file2:
    text2 = file2.read()
#pat = re.findall(r"\d{4}.\d[0-31].\d{2}", text2)

#print(re.sub(r'([1-2][0-9][0-9][0-9])/([0-3][0-9])/([0][0-9]|[1][0-2])', '\\2/\\3/\\1', text2))
