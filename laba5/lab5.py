import random
import re

with open("file1.txt", "w") as f:
    f.write("Thou blind fool Love what dost thou to mine eyes\nThat they behold and see not what they see")
    f.write("\nThey know what beauty id see where it lies\nYet what the best is take the worst to be")


def change_str():
    with open("file1.txt", "r") as file1:
        result = re.sub(r"(\w+[aeiou]\b)", r'\1' + "ay", file1.read())  #производим замещение сырого рег. выр. в котором задаём. что в конце слова должен быть один из символов, что мы заменяем на этот посл. символ + наша констр.
        with open("file2.txt", "w") as file2:
            file2.write(result)

    with open("file2.txt", "r") as file2:
        result = re.sub(r"(\w+[pbkfvmzhtdln]\b)", r'\1' + "way", file2.read())
        with open("file2.txt", "w") as file22:
            file22.write(result)

    with open("file2.txt", "r") as file2:
        a = file2.read().split()   #разделяем текст на отдельные слова, что становятся в список
        #print(sorted(a, key=str.lower))    #сортируем без учёта регистра
        with open("file2.txt", "w") as file2_w:
            file2_w.write(str(sorted(a, key=str.lower)))


change_str()
# RESULT OF change_str(): ['andway', 'beauty', 'beay', 'beholdway', 'bestway', 'blindway', 'dostway', 'eyes', 'foolway', 'idway', 'is', 'itway', 'know', 'lies', 'Loveay', 'mineay', 'notway', 'seeay', 'seeay', 'seeay', 'takeay', 'Thatway', 'theay', 'theay', 'they', 'they', 'They', 'Thouay', 'thouay', 'toay', 'toay', 'whatway', 'whatway', 'whatway', 'whatway', 'whereay', 'worstway', 'Yetway']

firsts = range(0, 20, 5)
seconds = range(3, 6)
toghethers = [(first, second) for first in firsts for second in seconds]
for toghether in toghethers:
    print(toghether)
# RESULT OF fourth():
# (0, 3)
# (0, 4)
# (0, 5)
# (5, 3)
# (5, 4)
# (5, 5)
# (10, 3)
# (10, 4)
# (10, 5)
# (15, 3)
# (15, 4)
# (15, 5)