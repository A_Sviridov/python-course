import re

texts = "In February 2021 Gorman was highlighted in Time magazine 100 Next List under the category of Phenoms"
x = re.findall(r"\b\w+\b", texts)
print(sorted(x, key=str.lower))

y = re.findall(r"\b\w+\b", texts)
print(sorted(y, key=str.lower, reverse=True))

z = dict(zip(y, reversed(x)))
print(z)

text_2 = "To explore different supervised learning algorithms, we're going to use aп algorithms of small synthetic or\
artificial datasets as examples, together with some larger real world datasets"
action = {word: len(word) for word in text_2.split()}

print(sorted(action.keys(), key=len, reverse=True))