import random


def names_surnames():
    name = input("Enter name: ")
    if name == "Joe":
        s_list = ["Warren", "Rixner", "Greiner", "Wong"]
        print("Hello, " + name, random.choice(s_list) + ", nice to see you again!")
    elif name == "Scott":
        s_list = ["Vechter", "Rhino", "Genigger", "Dronong"]
        print("Hello, " + name, random.choice(s_list) + ", nice to see you again!")
    elif name == "John":
        s_list = ["Williams", "Rover", "Grosser", "Kink"]
        print("Hello, " + name, random.choice(s_list) + ", nice to see you again!")
    elif name == "Stephen":
        s_list = ["Wanka", "Rollen", "Ginger", "Wronang"]
        print("Hello, " + name, random.choice(s_list) + ", nice to see you again!")
    else:
        print("Check list error, your name isn`t on list.")


names_surnames()
